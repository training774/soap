package com.soap.testsoap.endpoint;



import com.soap.testsoap.loancheck.Acknowledgement;
import com.soap.testsoap.loancheck.CustomerRequest;
import com.soap.testsoap.service.LoanService;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.ws.server.endpoint.annotation.Endpoint;
    import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
    import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
    public class LoanEndpoint {
        public static final String NAMESPACE = "http://www.soap.com/testsoap/loanCheck";
        @Autowired
        LoanService service;

        @PayloadRoot(namespace = NAMESPACE,localPart = "CustomerRequest")
        @ResponsePayload
        public Acknowledgement getLoc(@RequestPayload CustomerRequest request){
            return service.checkLoan(request);
        }
    }
